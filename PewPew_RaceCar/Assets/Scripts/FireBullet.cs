﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

    public Transform bulletOrigin;
    public GameObject bulletPrefab;
    public ParticleSystem  fire1, fire2;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("space"))
        {
            Fire();
        }

        if (Input.GetKey("up") || Input.GetKey ("w"))
        {
            fire1.Play();
            fire2.Play();
        }
        else

            fire1.Stop();
            fire2.Stop();  



		
	}

    public void Fire()
    {
        Instantiate(bulletPrefab, bulletOrigin.transform.position, bulletOrigin.transform.rotation);

    }



}
