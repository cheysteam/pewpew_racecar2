﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform player;

    private float enemySpeed ;
    public float eSpeed = 0.0002f;
    // Use this for initialization
    void Start () {



		
	}
	
	// Update is called once per frame
	void Update () {



        enemySpeed = eSpeed * Time.deltaTime;

        transform.position = Vector3.MoveTowards(transform.position, player.position, enemySpeed);

		
	}
}
