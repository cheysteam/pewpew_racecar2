﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public float speed;
    public float bulletTime;
    public float deathTime=5;
  

	// Use this for initialization
	void Start () {



		
	}
	
	// Update is called once per frame
	void Update () {


        bulletTime += Time.deltaTime;

        transform.Translate(Vector3.forward * speed);


        if (bulletTime > deathTime)
        {
            Death();
        }


		
	}


    public void Death()
    {

        Destroy(this.gameObject); 

    }

    private void OnCollisionEnter(Collision collision)
    {
        Death();
    }
}
