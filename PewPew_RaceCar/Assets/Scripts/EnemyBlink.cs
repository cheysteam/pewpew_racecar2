﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBlink : MonoBehaviour {

    private GameObject[] eyeOn;

    public float eyeTimer = 0; //timer for the eye

   // private GameObject[] eyeOff;

	// Use this for initialization
	void Start () {

        eyeOn = GameObject.FindGameObjectsWithTag("OnEye");

        for (int i = 0; i < eyeOn.Length; i++)
        {
            eyeOn[i].SetActive(false);
        }

        // eyeOff = GameObject.FindGameObjectsWithTag("OffEye");

    }
	
	// Update is called once per frame
	void Update () {

        eyeTimer += Time.deltaTime;

        if(eyeTimer < 1.6)
        {
            for(int i = 0; i<eyeOn.Length; i++)
            {
                eyeOn[i].SetActive(false);
            }
        }

        if(eyeTimer >= 1.6)
        {
            for (int i = 0; i < eyeOn.Length; i++)
            {
                eyeOn[i].SetActive(true);
            }
        }

        if(eyeTimer >= 3)
        {
            eyeTimer = 0;
        }	
	}


    void TurnEyeOn()
    { 

    }

    void TurnEyeOff()
    {

    }



}
