﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onTriggerPlay : MonoBehaviour {

    public List<GameObject> videos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            foreach (GameObject obj in videos)
                obj.SetActive(true);
        }
    }
}
